# for graph function

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import ast
import sys
import os
import optparse
import random


parser = optparse.OptionParser()

parser.add_option('--compile',  action='store_true', dest='compile')

options, args = parser.parse_args()

if options.compile:
    os.system("g++ main.cpp -o rstar")

os.system("rm output_file.txt")
os.system("./rstar")


def make_rectangles():
    file_coords = open('output_file.txt', 'r')
    rectangles = []
    r = lambda: random.randint(0, 255)
    color = (r(), r(), r())
    for coord in file_coords.read().split('\n'):
        if coord == "&end":
            r = lambda: random.randint(0, 255)
            color = (r(), r(), r())
            continue
        if coord is not '':
            obj_parsed = ast.literal_eval(coord)
            rectangles.append(
                patches.Rectangle(
                    obj_parsed[0], obj_parsed[1], obj_parsed[2], fill=False,
                    label="label", edgecolor='#%02X%02X%02X' % color)
            )
    return rectangles

# fig3 = plt.figure()
# ax3 = fig3.add_subplot(111)
rectangles = make_rectangles()
# for p in rectangles:
#     ax3.add_patch(p)
# fig3.savefig('rect3.png', bbox_inches='tight')

fig = plt.figure()
ax = fig.add_subplot(111)
for p in rectangles:
    ax.add_patch(p)
plt.xlim([-10, 80])
plt.ylim([-10, 80])
plt.show()
